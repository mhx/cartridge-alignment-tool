all: aligntool.pdf

aligntool.pdf: aligntool.ps
	@ps2pdf $< $@

realclean:
	@rm -f aligntool.pdf
