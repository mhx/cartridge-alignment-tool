aligntool
=========

This repo contains the postscript code to generate a tool to properly
align cartridges for turntables. When I originally wrote this, I was in
need of a such a calibration tool, but didn't want to pay extortionate
prices for existing ones. Also, I wanted to learn more about the theory
behind cartridge alignment.

Features
--------

There's actually a bunch of features that I haven't really seen with
other similar tools yet.

* You can fully customize your null points. By default, the code uses
  the null points suggested by ISO/IEC 60098-1987, but really, you're
  free to pick whatever you think makes sense for your setup.

* You get a nice plot of the tracking error relative to the tonearm
  position. This will be correct if you update the distance between
  the turntable center and tonearm pivot point.

* Optimum overhang, offset angle, and effective tonearm length will
  be computed automatically.

* There's rulers on the printout to help you verify that your printer
  is set up correctly.

* There's a small illustration to help you figure out whether you need
  to increase or decrease the overhang. This is really useful if you
  don't align cartridges for a living.

You can download a PDF generated from the default version from the repo.
